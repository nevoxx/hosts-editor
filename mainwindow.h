#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QDebug>
#include <QMessageBox>
#include <QFileInfo>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnLoad_clicked();
    void on_btnSave_clicked();

private:
    Ui::MainWindow *ui;
    QString currentFilePath;

    QString loadFileContents(QString filePath);
    void writeFileContents(QString filePath, QString content);

    QString getTableDataAsString();
    QStringList prepareTableRows(QString data);
    void renderTable(QStringList rows);

    bool checkFile();
};

#endif // MAINWINDOW_H
