#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    // VERY ugly solution, perhaps use Q_OS_WIN etc. ?
    #ifdef WIN32
        currentFilePath = "C:\\Windows\\System32\\drivers\\etc\\hosts";
    #else
        currentFilePath = "/etc/hosts";
    #endif


    ui->inputHostsPath->setText(currentFilePath);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnLoad_clicked()
{
    currentFilePath = ui->inputHostsPath->text().trimmed();

    // todo: this sucks right now, refactor later ...

    if (checkFile()) {
        QString hostsContent = loadFileContents(currentFilePath);
        QStringList hostsRows = prepareTableRows(hostsContent);

        renderTable(hostsRows);

        ui->btnSave->setEnabled(true);
    } else {
        QMessageBox msg(
            QMessageBox::Warning,
            "Error!",
            "The specifiel File Path does not exist. Aborting."
        );

        msg.exec();
    }
}

QString MainWindow::loadFileContents(QString filePath)
{
    QString contents = "";

    auto f = new QFile(filePath);

    f->open(QFile::ReadOnly);

    contents = QString(f->readAll());

    f->close();

    delete f;

    return contents;
}

void MainWindow::renderTable(QStringList rows)
{
    // setup table ...

    ui->tblHosts->setRowCount(0); // truncate

    auto header = QStringList();

    header.append("IP");
    header.append("Hostname");

    ui->tblHosts->setColumnCount(2);
    ui->tblHosts->setHorizontalHeaderLabels(header);
    ui->tblHosts->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    // fill table

    int i = 0;

    foreach (QString line, rows) {
        auto firstSpacePos = line.indexOf(' ');

        ui->tblHosts->insertRow(i);
        ui->tblHosts->setItem(i, 0, new QTableWidgetItem(line.mid(0, firstSpacePos).trimmed()));
        ui->tblHosts->setItem(i, 1, new QTableWidgetItem(line.mid(firstSpacePos).trimmed()));

        i++;
    }
}

void MainWindow::on_btnSave_clicked()
{
    QString hostsContent = getTableDataAsString();

    writeFileContents(currentFilePath, hostsContent);


    QMessageBox msg(
        QMessageBox::Information,
        "Success!",
        "The hosts file was saved successfull."
    );

    msg.exec();
}

QString MainWindow::getTableDataAsString()
{
    auto rows = QStringList();

    for (int i = 0; i < ui->tblHosts->rowCount(); i++) {
        auto ip = ui->tblHosts->item(i, 0)->text().trimmed();
        auto host = ui->tblHosts->item(i, 1)->text().trimmed();

        rows.append(ip + "\t" + host);
    }

    return rows.join("\n").trimmed();
}

QStringList MainWindow::prepareTableRows(QString content)
{
    // prepare data
    QStringList lines = content.split("\n");
    QStringList linesPrepared = QStringList();

    foreach (QString line, lines) {
        if (line.trimmed().length() > 0 && line.trimmed().at(0) != '#') {
            linesPrepared.push_back(line.trimmed().replace('\t', ' '));
        }
    }

    return linesPrepared;
}

void MainWindow::writeFileContents(QString filePath, QString content)
{
    auto f = new QFile(filePath);

    f->open(QFile::WriteOnly);

    f->write(content.toUtf8());

    f->close();

    delete f;
}

bool MainWindow::checkFile()
{
    QFileInfo checkFile(currentFilePath);

    return checkFile.exists() && checkFile.isFile();
}
